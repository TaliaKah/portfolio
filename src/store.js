import { createStore } from "vuex";
export default createStore({
  state() {
    return {
      page: "Home",
    };
  },
  mutations: {
    mutate_page(state, page) {
      state.page = page;
    },
  },
});
