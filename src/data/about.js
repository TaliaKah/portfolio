export default [
  `Actuellement en deuxième année de formation d'ingénieur créatif à l'IMAC (Image Multimédia Audiovisuel Communication),
  Talia'Kah est mon pseudonyme et le nom de la micro-entreprise que j’ai créée pour exercer en freelance dans les domaines d'autres créations artistiques, de la programmation informatique et d'autres enseignements. 
  J'aime toucher à tout et apprendre. 
  Je m’intéresse à un grand nombre de domaines allant des arts aux mathématiques en passant par la politique. Je suis toujours à la recherche de nouveaux projets qui me permettront de développer mes compétences ou d’en aquérir de nouvelles.
  N’hésitez pas à me contacter!
    `,
];
