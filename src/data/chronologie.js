export default {
  Présentation: [
    `
      Je m’appelle Anaïs Sarata Gougne.
      Je suis toujours à la recherche de nouveaux projets qui me permettront de développer mes compétences ou d’en aquérir de nouvelles.
      N’hésitez pas à me contacter!`,
  ],
  "Expériences Professionnelles": [
    {
      job: "Tutorat",
      additional:
        "En charge du soutien math à L’IMAC pour les premières années.",
      boss: "Université Gustave Eiffel",
      period: "septembre 2021 - en cours",
    },
    {
      job: "Mycelium Opera",
      additional:
        "Conception, fabrication et exposition d'une installation artistique",
      boss: "Université Gustave Eiffel",
      period: "janvier 2022 - mai 2022",
    },
    {
      job: "Stage en Programmation",
      additional: "Développement informatique sur un logiciel d’art génératif",
      boss: "CoolLab",
      period: "juin 2022 - septembre 2022",
    },
  ],
  "Contrats réalisés en freelance": [
    {
      job: "Audiovisuel",
      additional:
        "Réalisation et écriture de vidéos courtes me mettant en scène pour parler du numérique à destination d'un public en recherche de son orientation scolaire",
      boss: "Association Talents du Numérique",
      period: "janvier 2022 - en cours",
    },
  ],
  Événements: [
    {
      event: "23H BD",
      date: "24 - 25 mars 2022",
    },
    {
      event: "Game Jam Color",
      date: "18 - 20 février 2022",
    },
    {
      event: "Weekend Ingénieur Créatif",
      date: "19 - 20 février 2022",
    },
    {
      event: "Weekend Femme Ingénieure",
      date: "12 - 13 février 2022",
    },
    {
      event: "48H du court-métrage",
      date: "20 - 22 novembre 2020",
    },
    {
      event: "803Z-ink (ink-tober)",
      date: "octobre 2020",
    },
  ],
  "Formation et diplômes": [
    {
      diploma: "Formation d’ingénieur IMAC 2e année",
      additional: `(Image Multimédia Audiovisuel Communication)
      actuellement en année de césure`,
      establishment: "Université Gustave Eiffel",
      place: "Champs-sur Marne",
      date: "2020 - 2022",
    },
    {
      diploma: "Classe préparatoire MPSI-MP",
      establishment: "Lycée Claude Fauriel",
      place: "Saint-Etienne",
      date: "2018 - 2020",
    },
    {
      diploma: `Bacalauréat scientifique SVT spécialité ISN
      mention très bien section europèenne anglais`,
      establishment: "Lycée Georges Brassens",
      place: "Rive de Gier",
      date: "2018",
    },
  ],
  "Expérience étranger": [
    {
      explaination: "Résidence au Burkina Faso",
      period: "2009 - 2014",
    },
  ],
};
