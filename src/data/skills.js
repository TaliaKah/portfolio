export default {
  "Langages informatique": [
    {
      image: "python.svg",
      name: "Python",
    },
    {
      image: "cpp.png",
      name: "C++",
    },
    {
      image: "opengl.png",
      name: "OpenGL",
    },
    {
      image: "ocaml.svg",
      name: "Ocaml",
    },
    {
      image: "git.svg",
      name: "git",
    },
    {
      image: "html.svg",
      name: "HTML",
    },
    {
      image: "css.svg",
      name: "CSS",
    },
    {
      image: "javascript.svg",
      name: "JavaScript",
    },
    {
      image: "php.svg",
      name: "PHP",
    },
    {
      image: "mysql.svg",
      name: "MySQL",
    },
    {
      image: "p5js.png",
      name: "p5js",
    },
    {
      image: "vue.svg",
      name: "Vue",
    },
  ],
  Logiciels: [
    {
      image: "id.svg",
      name: "Adobe InDesign",
    },
    {
      image: "ae.svg",
      name: "Adobe AfterEffect",
    },
    {
      image: "ai.png",
      name: "Adobe Illustrator",
    },
    {
      image: "pr.svg",
      name: "Adobe Premiere Pro",
    },
    {
      image: "ps.svg",
      name: "Adobe Photoshop",
    },
    {
      image: "blender.png",
      name: "Blender",
    },
    {
      image: "TouchDesigner.png",
      name: "TouchDesigner",
    },
    {
      image: "bob.png",
      name: "CoolLab",
    },
    {
      image: "clip_studio_paint.png",
      name: "Clip Studio Paint",
    },
    {
      image: "krita.png",
      name: "Krita",
    },
    {
      image: "inkscape.svg",
      name: "Inkscape",
    },
  ],
};
