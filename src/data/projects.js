export default {
  "3D": [
    {
      name: "Sister Katherine",
      context: `Title Sequence fait en 3D à l'aide de Blender d'un film d'horreur.

      Ce film fictif, que nous avons sorti de notre imaginaire, raconte l'histoire de James. Après son passage sur l'île de Saint-Thomas, James ramène un chapelet acheté sur place.
      Quelques jours plus tard sa soeur tente de mettre fin à ses jours.
      James décide alors de mener l'enquête qui va le plonger dans l'histoire ancienne d'un orphelinat autrefois présent sur l'île.
       `,
      thumbnail: "katherine.png",
      video_link: "https://www.youtube.com/embed/tFBptM9r8d0",
      group: ["Clemence Voegelé", "Anaïs Sarata Gougne"],
      date: "avril-juin 2022",
    },
    {
      name: "Nature morte",
      context: `
            Ma première réalisation d'une scène complexe en 3D à l'aide de Blender.
            Dans un sous-sol éclairé à la bougie, une sorte de rituel à base de sang semble avoir eu lieu.
            `,
      thumbnail: "nature_morte.png",
      image: "3D/Anaïs_Sarata_Gougne.png",
      date: "Janvier 2022",
    },
    {
      name: "Escalier",
      context: "Escalier dans une ambiance sombre.",
      thumbnail: "escalier.png",
      image: "3D/escalier.png",
      date: "Novembre 2021",
    },
    {
      name: "Chaînes",
      context: "Modélisation d'une chaîne.",
      thumbnail: "chaînes.png",
      image: "3D/chaînes.png",
      date: "Novembre 2021",
    },
  ],
  Audiovisuel: [
    {
      name: "Porte à porte",
      context: `Le bureau des arts de notre campus organisait un 48h du court-métrage.
      Soit 2 jours pour imaginer, réaliser et monter un court-métrage selon un thème et des contraintes. 
      Le thème était le présent. 
      La contrainte était de faire un placement de produit pour une entreprise fictive appelée octante3z.
      Nous avions également la contrainte d'équipe d'utiliser un bruitage de brosse à dent.

      Notre personnage principal sort de son appartement pour aller faire les cours. En rentrant impossible de retrouver la bonne porte.

      Nous étions 6 et avions baptisé notre équipe la ClémenTeam, c'est pour cette raison que vous verrez souvent ce fameux fruit s'immiscer dans le court-métrage.
      J'ai participé à l'écriture du scénario et au tournage. J'ai joué, et j'ai aussi eu l'occasion de tenir la caméra et de toucher un peu au montage. 
      `,
      thumbnail: "porte_a_porte.png",
      group: [
        "Enguerrand De Smet",
        "Maxime Doyen",
        "Jules Fouchy",
        "Anaïs Sarata Gougne",
        "Lisa Pottier",
        "Clémence Voegelé",
      ],
      video_link: "https://www.youtube.com/embed/5xPWqGNKWeI",
      date: "20-22 novembre 2020",
    },
    {
      name: "Lucie.ia",
      context: `Court-métrage sur le thème du transhumanisme.

      Lucie possède une grande intelligence mais c'est une personne un peu étrange qui a du mal à s'exprimer et à mettre ses projets en avant. 
      Un jour une idée brillante va s'inviter dans son esprit pour chambouler sa vie.
      
      Nous étions une équipe de 5.
      J'ai participé à l'écriture du scénario et au découpage technique. J'ai également eu l'honneur d'interpréter Lucie, le personnage principal.
      Ce fut une expérience très enrichissante.

      Actuellement le court-métrage n'a pas encore été uploadé sur YouTube, mais il arrive.`,
      thumbnail: "lucie.ia.png",
      group: [
        "Laura Bonhomme",
        "Candice Branchereau",
        "Lucas David",
        "Maxime Doyen",
        "Anaïs Sarata Gougne",
      ],
      video_link: "https://www.youtube.com/embed/88BoewSD1tU",
      date: "décembre 2021",
    },
  ],
  Dessin: [
    {
      name: "Le royaume des couleurs",
      context: `Réalisé durant l'édition 2022 des 23H de la BD avec pour thème mythologie et anachronisme. 
      Le but était de réaliser 24 planches en 23 heures avec pour contrainte d'intégrer un personnage ou un objet de couleur bleu dans l'histoire.
      
      Dans un monde coloré où chaque personne est hiérarchisée en fonction de sa couleur, le bleu domine. 
      Néanmoins cela risque d'être vite remis en cause.
      
      Je mettrais bientôt la BD complète en ligne.`,
      thumbnail: "23h.jpg",
      date: "24-25 mars 2022",
      image: "Dessin/23h/0 titre.jpg",
    },
    {
      name: "PutinEscape",
      context: `Vous incarnez un diplomate ukrainien dont le rôle est au départ de convaincre Vladimir Poutine de ne pas attaquer le pays. 
      Malheureusement, comme l'actualité nous l'a montré, tout ne se passe pas comme prévu. 
      Dans un univers entre réalité et absurde votre séjour au Kremlin risque d'être fort en retournement de situation.

      Nous avons créé cet Escape Game sur genially. 
      J'ai fais la partie graphisme et ai contribué à inventer les énigmes ainsi qu'à écrire l'histoire.`,
      thumbnail: "putine_escape.jpg",
      game_link:
        "https://view.genial.ly/6225c021f85cd100133c92ac?idSlide=5b4600d0e8e8224ff4ba9ee7",
      date: "mars-mai 2022",
      image: "Dessin/PutinEscape/illustraion-of-the-game.jpg",
    },
    {
      name: "Tablette Graphique",
      context: `Voici quelques dessins fait à l'aide de ma tablette graphique sur des logiciels comme Clip Studio Paint, Krita et Painter Essential.`,
      thumbnail: "tablette.png",
      date: "",
      images: [
        "Dessin/tablette/serpent.png",
        "Dessin/tablette/dragon.jpg",
        "Dessin/tablette/arbre.png",
      ],
    },
    {
      name: "Aquarelle",
      context: `Voici quelques dessins fait à l'aquarelle. 
      L'un est une fée entourée des esprits de la nature. 
      L'autre un dessin fait pour me présenter où pleins de détails se cachent. `,
      thumbnail: "aquarelle.png",
      date: "",
      images: ["Dessin/aquarelle/presentation.png", "Dessin/aquarelle/fee.jpg"],
    },
    {
      name: "Crayon de papier",
      context: `Voici quelques dessins inachevés fait au crayon de papier. 
      Une guérrière et un bâtiment. `,
      thumbnail: "crayon.jpg",
      date: "",
      images: [
        "Dessin/crayon_papier/guerrierre_crayonne.jpg",
        "Dessin/crayon_papier/chateau_crayonne.jpg",
      ],
    },
    {
      name: "Autres techniques",
      context: `2 dessins sur papier noir, un avec de la peinture et l'autre avec de la craie grasse. 
      Le dernier est fait à l'encre de Chine. `,
      thumbnail: "autres.jpg",
      date: "",
      images: [
        "Dessin/autres/encre_chine.jpg",
        "Dessin/autres/damenature.jpg",
        "Dessin/autres/ballerine.jpg",
      ],
    },
    {
      name: "803Z-ink",
      context: `803Z est le bureau des arts de mon université. 
                Durant l'événement, nous étions au défi de faire un dessin par jour. 
                Chaque jour un nouveau mot était donné pour faire un nouveau dessin. 
                Le rythme n'était pas facile à tenir mais j'ai réussi le défi en publiant chaque jour un nouveau dessin.`,
      thumbnail: "803zink.jpeg",
      date: "octobre 2020",
      images: [
        "Dessin/803Zink/contraste.jpeg",
        "Dessin/803Zink/enfermé.e.jpeg",
        "Dessin/803Zink/instantané.jpeg",
        "Dessin/803Zink/coeur.jpeg",
        "Dessin/803Zink/carte.jpeg",
        "Dessin/803Zink/négatif.jpeg",
        "Dessin/803Zink/superposition.jpeg",
        "Dessin/803Zink/3D.jpeg",
      ],
    },
    {
      name: "Carte du château de Champs-Sur-Marne",
      context: `
      En mon binôme nous avons décidé de faire la carte du château de Champs-Sur-Marne à destination des enfants.
      J'ai fait les illustrations pour la légende avec Illustrator.`,
      thumbnail: "carte.svg",
      date: "2020",
      group: ["Adam Fereira", "Anaïs Sarata Gougne"],
      images: [
        "Dessin/carte/chaussures.svg",
        "Dessin/carte/crayon.svg",
        "Dessin/carte/bousssole.svg",
        "Dessin/carte/fontaine.svg",
        "Dessin/carte/loupe.svg",
        "Dessin/carte/pq.svg",
        "Dessin/carte/statue.svg",
        "Dessin/carte/châteausvg.png",
      ],
    },
    {
      name: "Vierge Grandeur Nature",
      context: `Ma belle-mère, impliquée dans la vie religieuse de son village,
      m'a mis au défi de réaliser la Vierge Marie à ma taille. `,
      thumbnail: "vierge.jpg",
      date: "2019",
      image: "Dessin/vierge/vierge.jpg",
    },
    {
      name: "Caricatures",
      context: `Durant le premier confinement j'ai voulu tourner en dérision l'actualité.`,
      thumbnail: "caricatures.jpg",
      date: "2020",
      images: [
        "Dessin/caricature/planete_covid.jpg",
        "Dessin/caricature/oeuf_covid.jpg",
        "Dessin/caricature/marianne.jpg",
      ],
    },
  ],
  Programmation: [
    {
      name: "IMACRUN",
      context: `
      Nous avons codé un jeu ressemblant à TempleRun en C++ avec OpenGL. Nous nous sommes servis de SDL2.

      Dans le jeu nous sommes un petit astraunote qui doit rejoindre le bout du parcours, il peut attraper des bouteilles d'oxygène.

      J'ai grandement participé à la partié code en permettant au personnage de sauter et d'attraper des bouteilles. 
      J'ai aussi fait en sorte qu'on puisse rejouer plusieurs fois. 
      À la fin le nombre de bouteilles attrappées apparaît dans le terminal. `,
      thumbnail: "imacrun.png",
      group: ["Théo Clément", "Lucas David", "Anaïs Sarata Gougne"],
      link: "https://gitlab.com/lucas29.david/IMACRUN",
      date: "octobre-décembre 2021",
      image: "Programmation/imacrun.png",
    },
    {
      name: "Twitter's Iceberg",
      context: `Le but de ce projet était de mettre en lumière les aspects cachés de de l'algorithme de Twitter.
    On peut parler de la face immergée de l'iceberg, d'où le travail graphique en ce sens.  
    De plus les couleurs bleu et blanc permettent un fort contraste, tout en rappelant Twitter et les couleurs d'un iceberg dans la mer.
    J'ai fait la partie programmation du projet à l'aide de p5js.
    On appuie sur un bouton qui fait tomber une phrase. Cela permet d'enfoncer l'iceberg dans l'eau par le poids de la phrase pour faire découvrir d'autres mots.
    Lorsque l'iceberg est entièrement sous l'eau on peut appuyer sur le bouton reset et recommencer.
     `,
      thumbnail: "iceberg.png",
      group: ["Marie Guillot", "Clémence Voegelé", "Anaïs Sarata Gougne"],
      game_link: "https://taliakah.github.io/iceberg-of-twitter/",
      date: "mai 2022",
      image: "Programmation/iceberg.png",
    },
    {
      name: "Childchemy",
      context: `Ce jeu a été fait durant l'évènement Game Jam color organisé par le bureau des arts de l'université.
      Le principe est de coder un jeu en 48 heures d'après un thème, récréation cette année. 
      La contrainte était que les graphismes soit principalement dans la couleur choisie par l'équipe lors de l'inscription, l'ocre pour nous.

      Nous incarnons un élève de primaire qui va tenter de se sortir de certaines situations compliqués.

      Nous étions une équipe avec beaucoup de personnes ce qui nous a permis de diviser le travail en fonction des capacités de chacun (programmation, graphisme, gameplay).
      Personnellement j'ai principalement participé èà la partie game play en écrivant l'histoire et les dialogues. J'ai aussi touché au code.
      Le jeu a été codé avec svelte qui était une découverte pour moi.`,
      thumbnail: "childchemy.png",
      group: [
        "Enguerrand De Smet",
        "Jules Fouchy",
        "Lilou",
        "Amalia",
        "Anaïs Sarata Gougne",
        "Aurore Lafaurie",
        "Clémence Voegelé",
        "Emma Pernin",
      ],
      game_link: "https://dsmte.github.io/GameJamColor2022/",
      date: "18-20 février 2022",
      image: "Programmation/chilchemy.png",
    },
    {
      name: "Portfolio",
      context: `J'ai fait ce portfolio à l'aide de Vuejs. 
      Le défi était de faire un portfolio en 48 heures.
      Au bout de ces 48 heures, le portfolio était publié avec toute sa structure.
      Je l'ai par la suite modifié, en changeant le CSS pour le rendre plus agréable à la lecture.`,
      thumbnail: "portfolio.png",
      date: "mars 2022",
      image: "Programmation/logo.png",
    },
    {
      name: "Yu-Gi-Oh!",
      context: `Glossaire des cartes Yu-Gi-oh! fait avec Vue.js.
      Le site nécessite quelques réparations.`,
      thumbnail: "yugi.png",
      website_link: "https://taliakah.gitlab.io/yugioh/",
      date: "mars-juin 2022",
      image: "Programmation/yugi.png",
    },
    {
      name: "Covid",
      context: ` J'ai codé à l'aide de p5js une alerte à propos du covid.
      J'ai choisi la police Helvitica pour la lisibilité des informations.
       J'ai pris différentes tailles pour chaque partie du texte pour mettre en valeurs les différentes informations en fonction de leur importance. 
       J'ai choisi de faire apparaître les chiffres en rouge pour qu'ils attirent l'oeil. 
       Le rouge, le blanc et le noir permet un grand contraste et font apparaître la gravité de la situation. 
       La source et en petit italique pour que le spectateur soit d'abord concentré sur l'information.
       Les informations arrivent sur un rythme rapide pour interpeller et stimuler le spectateur pendant ces quelques secondes où le texte apparaît. 
       Le nombre de cas arrive en grossissant du fond de manière agressive pour que le spectateur se sente acculé par l'information. 
       Le pourcentage depuis 14 jours apparaît doucement, mais les couleurs donnent à cette apparition un air de gravité. 
       Je fais boucler l'information pour que le spectateur puisse profiter de cet effet agressif et de gravité à l'apparition du texte.`,
      thumbnail: "covid.png",
      website_link: "https://editor.p5js.org/TaliaKah/sketches/n-uypImPx",
      date: "avril 2022",
      image: "Programmation/covid.png",
    },
    {
      name: "Trouve ton député",
      context: `Mon premier projet de Web. Nous devions utiliser une API qui nous intéressait pour faire un site web.
      Ayant un fort intérêt pour la politique avec mon binôme, nous avons naturellement choisi une API du gouvernement.
      Nous l'avons fait en 3 jours. J'ai fait toute la partie HTML et CSS.`,
      thumbnail: "depute.svg",
      group: ["Anaïs Sarata Gougne", "Yvan Smorag"],
      website_link: "https://taliakah.github.io/trouve-ton-depute/",
      date: "décembre 2020",
      image: "Programmation/logo5.svg",
    },

    {
      name: "Bezier",
      context: `Le but du projet était de modéliser des courbes de Bézier. 
      J'ai utilisé la librairie p6 pour l'affichage.`,
      thumbnail: "bezier.jpg",
      link: "https://gitlab.com/TaliaKah/bezier",
      date: "avril-juin 2022",
      image: "",
    },
    {
      name: "Escape Game",
      context: `La première fois que je découvrais la programmation à l'occasion de l'épreuve d'Informatique et Science du Numérique du baccalauréat 2018.
      Nous incarnons un personnage qui doit sortir d'un donjon.
      Nous avons codé 5 pièces et donné la possiblité de passer par des chemins différents pour finalement atteindre la sortie.
      Le jeu à été codé en Python à l'aide de la librarie pygame`,
      thumbnail: "isn.jpg",
      date: "2018",
      image: "Programmation/jeurègle.png",
    },
  ],
  "Installation Immersive": [
    {
      name: "Mycelium Opera",
      context: `
      Notre projet est à l'initiative d'un des membres de notre groupe de 4 personnes. L'université Gustave Eiffel nous avait alloué un budget de 1000 euros.
      L'idée était de faire une installation intéractive avec lumière, son et images.

      On est dans une pièce sombre avec des bâtons lumineux accrochés au plafond, construits et codés par nos soins. 
      Au bout de ses bâtons il y a des capteurs de mouvements relié au logiciel Touch Designer qui détectent les visiteurs.
      Sur trois murs il y a des projections faites par nos soins et une ambiance généré par ce même logiciel.
      Le concept de cette installation est de montrer l'intéraction de l'homme avec la nature. 
      Le mycélium est la partie souterraine de ce que l'on voit en surface, le champignon. C'est un réseau filaire qui relie tous les êtres de la nature. 
      Il leur permet de vivre ou les détruit. C'est lui qui gère le cycle de la nature.
      Ainsi nous invitons le visteurs sous terre à intéragir avec le mycélium représenté par les bâtons lumineux.
      Lorsque le visiteur passe rapidemment sous le bâton l'intéraction est brève la lumière du bâton change violemment. 
      L'homme ne fait pas attention à ce qui l'entoure, il agit dans la nature sans réfléchir aux conscéquences de ses actes.
      Lorsque le visiteur s'arrête longtemps sous un bâton, la couleur se répend progressivement aux autres bâtons pour former un îlot.
      L'homme prend le temps d'observer la nature et de se reconnecter à elle.
      Les projections et le son s'adapteront également. D'abord plus numérique et technologique, les projections deviendront plus organiques.

      Ce projet m'a permis de toucher vraiment à tout et d'apprendre pleins de nouvelles choses, 
      électronique, code arduino pour les leds des bâtons, l'utilisation de Touch Designer, etc.
      J'ai principalement participé à la génération des premières images, mais j'ai aussi touché à la construction des bâtons.
      Après nos trois semaines dédiées nous avons décidé de poursuivre le projet pour pouvoir l'exposer à la bibliotèque de notre campus en mai.
      `,
      thumbnail: "mycelium.png",
      group: [
        "Anaïs Sarata Gougne",
        "Victor Guichard",
        "Angélique Lebas",
        "Ulysse Roussel",
      ],
      link: "",
      date: "janvier-mai 2022",
      video: "Installation/cells_slicing.mp4",
    },
  ],
  Animation: [
    {
      name: "Pub baguette magique",
      context: `Le projet était de promouvoir un objet fictif à l'aide d'After Effect.
      J'ai choisi de faire une publicité sur une baguette magique. Je n'avais jamais eu l'occasion d'utiliser ce logiciel auparavant.`,
      thumbnail: "baguette.png",
      date: "2021",
      video: "Animation/baguette.mp4",
    },

    {
      name: "Colère",
      context: `Le but était de faire un gif exprimant une émotion.
      J'ai choisi la colère en prennant des images du clip vidéo de la chanson Marcia Baïla des Rita Mitsouko. 
      J’ai créé 16 images différentes avec le logiciel Krita, en ajoutant des éléments dessinés par mes soins. 

      En fond des éclairs apparaissent, ils symbolisent une colère foudroyante, brutale.
      La chanteuse se métamorphose. Ses yeux deviennent rouges de colère. Lorsque des flammes
      commencent à jaillir de ses doigts, le pentacle commence à se dessiner. Le pentacle à l’envers est le
      symbole de la magie noire dont le principe est de faire le mal, ce qui est destructeur
      comme la colère peut l’être.
      La métamorphose s’achève lorsque le pentacle est complet. Elle est hors d’elle. C’est une autre
      personne. La colère a ce pouvoir de métamorphoser l’individu en une créature maléfique.
      Le feu représente la spontanéité et la passion destructrice. Il est donc naturellement associé à la
      colère.

      Je réutilise les 2 dernières images plusieurs fois pour permettre d’observer plus longtemps
      l’émotion finale de l’individu, où seule les flammes de la colère bougent et le pentacle, symbole de
      la transformation, clignote.`,
      thumbnail: "colère.png",
      date: "2021",
      image: "Animation/colèremaléfique.gif",
    },
    {
      name: "I see you",
      context: `J'ai utilisé une Intelligence Artificielle, via GAN (Generative Adversarial Network), qui dessinait des portrait de personnages féminins d'anime, pour réaliser cette animation. 
      J'ai donné des valeurs extrêmes à l'IA pour lui faire génèrer des glitch. 

      Ici on a un personnage qui progressivement nait d'une flamme en nous regardant.
      Le personnage nous voit, réagit, puis se métamorphose.`,
      thumbnail: "i_see_you.png",
      date: "2021",
      video: "Animation/Iseeyou.mp4",
    },
  ],
  Autres: [
    {
      name: "Rumplestilskin",
      context: `Lecture d'un conte, auquel j'ai ajouté des bruitages pour rendre l'histoire plus vivante.
      Je n'avais jamais touché à un logiciel de traitement du son auparavant et ça m'a beaucoup plu d'avoir l'occasion de le faire.`,
      thumbnail: "rumple.png",
      date: "2021",
      sound: "Autres/Rumpelstiltskin.mp3",
    },
    {
      name: "Glitch artistique",
      context: `Avec mon binôme nous avons choisi de faire des glitch artistique et destructurant.
      Ici, une photo que j'ai prise puis modifiée avec des logiciels de retouche photographique dans le cadre de ce projet.`,
      thumbnail: "glitch.jpg",
      date: "2020",
      images: [
        "Autres/glitch/visuel.jpg",
        "Autres/glitch/visuelglitch.jpg",
        "Autres/glitch/4gli.jpg",
      ],
    },
    {
      name: "Sculptures",
      context: `Voici deux sculptures que j'ai faites. 
      L'une est un personnage fait en argile.
      L'autre est un arbre en fil de fer et résine.`,
      thumbnail: "sculpture.jpg",
      images: [
        "Autres/sculpture/arbre.jpg",
        "Autres/sculpture/corps_argile.jpg",
        "Autres/sculpture/tete_argile.jpg",
      ],
    },
    {
      name: "Perles",
      context: `Voici quelques-unes de mes créations en perles.
      Il y a des collier ras cou et des portes-clefs.`,
      thumbnail: "perles.jpg",
      images: [
        "Autres/perles/bijoux.jpg",
        "Autres/perles/porte_cle.jpg",
        "Autres/perles/bracelet.jpeg",
      ],
    },
    {
      name: "Maquillage Haloween",
      context: `Pour chaque Halloween j'aime associer certains de mes vêtements et accessoires pour avoir un rendu original.
      Je fais également un maquillage qui correspond au personnage que j'invente.`,
      thumbnail: "halloween.jpg",
      images: [
        "Autres/halloween/poupee_demoniaque_salle_de_bain.jpg",
        "Autres/halloween/visage_amour_vengeur.jpg",
        "Autres/halloween/servante.jpg",
        "Autres/halloween/poupee_demoniaque_make_up.jpg",
        "Autres/halloween/visage_sanglant.jpg",
        "Autres/halloween/amour_vengeur.jpg",
        "Autres/halloween/carnaval_sinistre.jpg",
        "Autres/halloween/pretresse.jpg",
      ],
    },
  ],
};
